# Emotion-Detection-In-Speech
Comparison of GMM-HMM and Deep Neural Network Implementations in their performance on the task of detecting emotion in speech.

Implemented and trained both GMM-HMM and Deep Neural Network (DNN) to detect 5 basic
emotions in human speech: Anger, Fear, Surprise Sadness and Happiness.
