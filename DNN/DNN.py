import neurolab as nl
import numpy as np
import time
import random

def sigmoid(z):
    """The sigmoid function."""
    return 1.0/(1.0+np.exp(-z))

sigmoid_vec = np.vectorize(sigmoid)

def get_activations(layer_no, data = [], index = 0, load = False):
	if load:
		return eval(open('layer'+str(layer_no)+'_activations.txt').read().strip())
	else:
		# print "loading layer", layer_no
		net = nl.load('layer_'+str(layer_no)+'.txt')
		a = data[index]
		weights = []
		biases = []
		biases.append(net.layers[0].np['b'])
		weights.append(net.layers[0].np['w'])
		biases = np.array(biases)
		weights = np.array(weights)
		a = sigmoid_vec(np.add(np.transpose(np.dot(weights[0], a)),biases))
		return a[0].tolist()

def layer_train(inp, units, layer_no, inp_range=(0, 255), epochs=30):
	net = nl.net.newff([inp_range] * len(inp[0]), [units, len(inp[0])])
	print type(inp), type(inp[0]), type(inp[0][0])
	print "layer_no = ", layer_no
	# net.trainf = nl.train.train_gdx
	err = net.train(inp, inp, show=1,epochs=epochs)	
	'''f = open("all_weights_biases.txt", a)
	print layer_no, str(list(net.layers[0].np['w'],net.layers[0].np['b']))
	f.write(str(list(net.layers[0].np['w'],net.layers[0].np['b'])))'''
	net.save('layer_'+str(layer_no)+'.txt')

def output_train(inp, units, layer_no, inp_range=(0, 255), epochs=30, target = [], flag=0):
	net = nl.net.newff([inp_range] * (len(inp[0])), [units])
	# net.trainf = nl.train.train_gdx
	if flag ==1:
		net.layers[-1].transf = nl.trans.LogSig()			#if flag is 1,output layer transfer function is Logistic Regression
	else:
		net.layers[-1].transf = nl.trans.SoftMax()			#if 0,SoftMax CLassifier is used
		
	# print target
	err = net.train(inp, target, show=1,epochs=65)			#Gradient Descent Backpropagation	
	
	net.save('layer_'+str(layer_no)+'.txt')

def test_one(data, layers):
	activation = []
	for layer in layers[1:]:
		'''
			returns the activation of the output layer
		'''
		#net=nl.load('layer_'+str(layer[0])+'.txt')
		#print('activation=',activation,"layer=",layer[0]-1)
		if  layer[0] == 1:
			activation = get_activations(layer[0], data = [data])
		elif layer == layers[-1]:
			net=nl.load('layer_'+str(layer[0])+'.txt')
			return net.sim([activation])[0].tolist()
		else:
			#print(activation)
			activation = get_activations(layer[0], data = [activation])

def train_model(layers, inp, target2):
	'''
		layers --> list of tuples, tuple is (layer_no, number of units, epochs)
	'''
	for layer in layers:
		if not layer[0] == 0:
			if  layer[0] == 1:
				layer_train(inp, layer[1], layer[0], inp_range = (-1,1), epochs = layer[2])
				d = inp
				# get_activations(layer[0])
			elif layer == layers[-1]:
				print "in output train"
				output_train(get_activations(layer[0]-1, load = True), layer[1], layer[0], inp_range = (-1,1), epochs = layer[2], target = target2)
			else:
				d = get_activations(layer[0]-1, load = True)
				layer_train(d, layer[1], layer[0], inp_range = (-1,1), epochs = layer[2])
				
			layer_no = layer[0]	
			activation_list = []
			f = open('layer'+str(layer_no)+'_activations.txt', 'w')
			if not layer == layers[-1]:
				for i in range(len(inp)):
					activation_list.append(get_activations(layer[0], index = i, data = d))
				f.write(str(activation_list))
				f.close()
		else:
			pass

if __name__ == '__main__':
	'''
		read all MFCCs
		send it to train_model
		then call test_one
	'''
	from berlindb import mfcc_extract as me
	data = me.extract()
	
	layers = [(0, 13 * 8, 500), (1, 25, 500), (2, 6, 100)]
	# layers = [(0, 13 * 5, 1000), (1, 15, 1000),(2, 8, 600),  (3, 6, 100)]
	train_data = me.nn_inp(13 * 8)
	# print
	inp1,target1 = zip(*train_data)
	print max([max(p) for p in inp1])
	print len(inp1), len(target1), target1[0]
	train_model(layers, inp1, target1)
	res=[]
	for i in range(500,1500):
		result = test_one(inp1[100], layers)
		res.append(result.index(max(result)))
	'''print inp1[100],inp1[900]
	result = test_one(inp1[100], layers)
	print result,target1[100],result.index(max(result))
	result = test_one(inp1[105], layers)
	print result,target1[105],result.index(max(result))
	print target1.index([1,0,0,0,0,0])
	print(set([tuple(l) for l in target1]))'''
	print res
	print set(res)
